public class DishWasher
{
	private String brand;
	private double size;
	private int time;
	
	private double dryness;
	private boolean validity;
	
	public String printBrand()
	{
		String brand = "Samsung Dish Washer is so much better than any other Dish Washers! It's cheaper, compact and most importantly a fastest Dish Washing machine";
		return brand;
	}
	public String fastClean()
	{
		String clean = "Samsung Dish Washer only takes 3 minutes to clean all the dishes!";
		return clean;
	}
	
	public double dryDishes (int numPlates)
	{
		if (0 < numPlates)
		{
			this.dryness = 99.99;
		}
		else 
		{
			this.dryness = 0;
		}
		return this.dryness;
	}
	
	private boolean helper (int numPlates)
	{
		if (numPlates > 0)
		{
			this.validity = true;
		}
		else 
		{
			this.validity = false;
		}
		return this.validity;
	}
	
	public void setBrand(String newBrand)
	{
		this.brand = newBrand;
	}
	public String getBrand()
	{
		return this.brand;
	}
	
	public void setSize(double newSize)
	{
		this.size = newSize;
	}
	public double getSize()
	{
		return this.size;
	}
	
	public void setTime(int newTime)
	{
		this.time = newTime;
	}
	public int getTime()
	{
		return this.time;
	}
	
	public void setDryness(double newDryness)
	{
		if (0 < newDryness)
		{
			this.dryness = 99.99;
		}
		else 
		{
			this.dryness = 0;
		}
	}
	public double getDryness()
	{
		return this.dryness;
	}
	
	public DishWasher(String inputBrand, double inputSize, int inputTime)
	{
		this.brand = inputBrand;
		this.size = inputSize;
		this.time = inputTime;
	}
}
