import java.util.Scanner;

public class ApplianceStore
{
	public static void main (String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		DishWasher[] appliances = new DishWasher[4];
		
		for (int i = 0; i<appliances.length; i++)
		{
			appliances[i] = new DishWasher(sc.next(),sc.nextDouble(),sc.nextInt());
			
		  /*appliances[i].setBrand(sc.next());	
			appliances[i].setSize(sc.nextDouble());		
			appliances[i].setTime(sc.nextInt());*/
		}
		
		//System.out.println(appliances[3]);
		
		System.out.println();		//these are just to create spaces so that things are less messy when you execute the program
		
		System.out.println(appliances[3].getBrand());	//before setter methods
		System.out.println(appliances[3].getSize());
		System.out.println(appliances[3].getTime());
		
		System.out.println();		//making sure there are spaces in between print statements
		
		appliances[3].setBrand(sc.next());		//setter methods to re-initialize the values to the thrid appliance.
		appliances[3].setSize(sc.nextDouble());
		appliances[3].setTime(sc.nextInt());
		
		System.out.println();
		
		System.out.println(appliances[3].getBrand());		//after setter methods
		System.out.println(appliances[3].getSize());
		System.out.println(appliances[3].getTime());
		
		//System.out.println(appliances[0].printBrand());
		//System.out.println(appliances[0].fastClean());
		
		//System.out.println(appliances[0].getBrand());	//print statements just to check if the getter method is returning the correct value;
		//System.out.println(appliances[0].getSize());
		//System.out.println(appliances[0].getTime());
		
		/*System.out.println("How many plates are there in the dish washer son?");		//Part 1 of Assignment 4
		int numPlates = sc.nextInt();
		System.out.println("The dish washer removed " + appliances[1].dryDishes(numPlates) + "% of surface water from all the dishes!");*/
		
		System.out.println();
		
		appliances[1].setDryness(sc.nextDouble());
		System.out.println("The dish washer removed " + appliances[1].getDryness() + "% of surface water from all the dishes!");
	}
}
